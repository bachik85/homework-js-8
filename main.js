const correctType = document.createElement('span');
const cancelButton = document.createElement('button');


document.getElementById('input').addEventListener('focus', () => {
    document.getElementById('input').style.outline = "2px solid green";
})

document.getElementById('input').addEventListener('blur', () => {
    const val = document.getElementById('input').value;
    if (val > 0) {
        correctType.innerHTML = `Текущая цена: ${val}`;
        cancelButton.innerText = 'x';
        cancelButton.style.margin = '5px';
        document.getElementById('correct-price-container').append(correctType, cancelButton);
        document.getElementById('error').innerText = ' ';
        document.getElementById('input').style.color = 'green';
    } else {
        document.getElementById('error').innerText = 'Please enter correct price';
        document.getElementById('error').style.color = 'red';
        document.getElementById('input').value = '';
        correctType.innerHTML = ' ';
        cancelButton.remove();
    }
});

cancelButton.addEventListener('click', () => {
    correctType.remove();
    cancelButton.remove();
    document.getElementById('input').value = '';

})

